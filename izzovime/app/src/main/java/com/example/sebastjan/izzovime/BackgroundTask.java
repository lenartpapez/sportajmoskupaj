package com.example.sebastjan.izzovime;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.widget.ListView;


import java.util.ArrayList;


public class BackgroundTask extends AsyncTask<String, Event, String>{
    Activity activity;
    Context ctx;
    ListView listview;
    BackgroundTask(Context ctx){
        this.ctx = ctx;
        activity = (Activity) ctx;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected String doInBackground(String... params) {
        DatabaseConnector db = new DatabaseConnector(ctx);
        String method = params[0];
        if(method.equals("add_info")){
            String naziv = params[1];
            String tip = params[2];
            String opis = params[3];
            String datum = params[4];
            String cas = params[5];
            String lat = params[6];
            String lng = params[7];
            String zoom = params[8];
            db.insertData(naziv, tip, opis, datum, cas, lat, lng, zoom);
            return "One row inserted";
        }
        return null;
    }

    @Override
    protected void onProgressUpdate(Event... values) {
    }

    @Override
    protected void onPostExecute(String s) {

    }

}
