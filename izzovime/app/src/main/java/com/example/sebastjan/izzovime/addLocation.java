package com.example.sebastjan.izzovime;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;


public class addLocation extends AppCompatActivity implements OnMapReadyCallback {

    GoogleMap gMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        SupportMapFragment mapFragment = (SupportMapFragment)
                getSupportFragmentManager()
                        .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        gMap = googleMap;
        LatLng Ljubljana = new LatLng(46.050760, 14.502806);
        CameraPosition cameraPosition = CameraPosition.builder()
                .zoom(14).tilt(20).bearing(90).target(Ljubljana).build();
        gMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        gMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override            public void onMapClick(LatLng point) {
                drawMarker(point);   // Draw marker
                double lat = point.latitude;
                double lng = point.longitude;
                float zoom = gMap.getCameraPosition().zoom;
                Intent location = getIntent();
                location.putExtra("lat", ""+lat);
                location.putExtra("lng", ""+lng);
                location.putExtra("zoom", ""+zoom);
                setResult(Activity.RESULT_OK, location);
                finish();
            }
        });
    }
    private void drawMarker(LatLng point){
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(point);
        gMap.addMarker(markerOptions);
    }

}