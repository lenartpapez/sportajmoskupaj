package com.example.sebastjan.izzovime;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TimePicker;

import java.util.Calendar;

public class findFilter extends AppCompatActivity implements View.OnTouchListener{
    public static Spinner s1;
    public static EditText dat, cas;
    private String[] sp1;
    public Button bt1;
    public int year, month, day, hour, min;
    static boolean cameFromFilter = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_find_filter);
        napolni();
        apply();
    }

    public void napolni(){
        this.sp1 = new String[] {""};

        s1 = (Spinner) findViewById(R.id.spinner);
        dat = (EditText) findViewById(R.id.editText7);
        cas = (EditText) findViewById(R.id.editText8);

        ArrayAdapter<String> adapter1 = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, sp1);

        s1.setAdapter(adapter1);

        s1.setOnTouchListener(this);
        dat.setOnTouchListener(this);
        cas.setOnTouchListener(this);

    }
    public void apply(){
        bt1 = (Button) findViewById(R.id.apply);
        bt1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent x = new Intent(findFilter.this, showLocations.class);
                cameFromFilter = true;
                x.putExtra("cameFromFilter", cameFromFilter);
                startActivity(x);
            }
        });
    }




    @Override
    public boolean onTouch(View v, MotionEvent event){
        if(v == s1){
            if (MotionEvent.ACTION_UP == event.getAction()) {
                this.sp1 = new String[]{"Borilne veščine", "Fitnes", "Kolesarjenje", "Košarka", "Nogomet", "Rekreacija", "Odbojka","Plavanje",
                                        "Ples", "Plezanje", "Pohodništvo", "Rokomet", "Rolanje", "Rolkanje", "Tek", "Tenis", "Drugo"};
                Spinner s1 = (Spinner) findViewById(R.id.spinner);
                ArrayAdapter<String> adapter1 = new ArrayAdapter<String>(this, R.layout.spinner_style, R.id.weekofday, sp1);
                s1.setAdapter(adapter1);
            }
        } else if(v == dat){
            if(MotionEvent.ACTION_UP == event.getAction()) {
                datt();
                dat.setShowSoftInputOnFocus(false);
            }
        }else if(v == cas){

            if(MotionEvent.ACTION_UP == event.getAction()) {
                cass();
                cas.setShowSoftInputOnFocus(false);
            }
        }
        return false;
    }

    public static String getTypeDetail() {
        return s1.getSelectedItem().toString();
    }

    public static String getDateDetail() {
        return dat.getText().toString();
    }

    public static String getTimeDetail() {
        return cas.getText().toString();
    }

    public void datt(){
        final Calendar c = Calendar.getInstance();
        year = c.get(Calendar.YEAR);
        month = c.get(Calendar.MONTH);
        day = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog dp = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                dat.setText(dayOfMonth + "." + (month + 1) + "." + year);
            }
        }, year, month, day);

        dp.show();
    }

    public void cass(){
        final Calendar c = Calendar.getInstance();
        hour = c.get(Calendar.HOUR_OF_DAY);
        min = c.get(Calendar.MINUTE);

        TimePickerDialog tp = new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                String toSet = String.format("%02d:%02d", hourOfDay, minute);
                cas.setText(toSet);
            }
        }, hour, min, true);

        tp.show();
    }
}
