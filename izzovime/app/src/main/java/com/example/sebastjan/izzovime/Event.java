package com.example.sebastjan.izzovime;

/**
 * Created by Sebastjan on 28. 12. 2016.
 */

public class Event {

    private String naziv, tip, opis, datum, cas, lat, lng, zoom;

    public Event(String naziv, String tip, String opis, String datum, String cas, String lat, String lng, String zoom) {
        this.setNaziv(naziv);
        this.setTip(tip);
        this.setOpis(opis);
        this.setDatum(datum);
        this.setCas(cas);
        this.setLat(lat);
        this.setLng(lng);
        this.setZoom(zoom);
    }

    public String getNaziv() {
        return naziv;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    public String getTip() {
        return tip;
    }

    public void setTip(String tip) {
        this.tip = tip;
    }

    public String getOpis() {
        return opis;
    }

    public void setOpis(String opis) {
        this.opis = opis;
    }

    public String getDatum() {
        return datum;
    }

    public void setDatum(String datum) {
        this.datum = datum;
    }

    public String getCas() {
        return cas;
    }

    public void setCas(String cas) {
        this.cas = cas;
    }

    public String getLat() { return lat;}

    public void setLat(String lat) { this.lat = lat; }

    public String getLng() { return lng; }

    public void setLng(String lng) { this.lng = lng; }

    public String getZoom() { return zoom; }

    public void setZoom(String zoom) { this.zoom = zoom; }
}
