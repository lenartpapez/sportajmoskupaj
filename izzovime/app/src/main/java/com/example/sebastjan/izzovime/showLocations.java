package com.example.sebastjan.izzovime;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import java.util.ArrayList;

public class showLocations extends FragmentActivity implements OnMapReadyCallback {


    private GoogleMap gMap;
    private String lat, lng, naziv;
    ArrayList<String> events;
    boolean cameFromFilter = false;
    boolean eventAdded = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_locations);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map2);
        mapFragment.getMapAsync(this);


    }

    @Override
    protected void onResume() {
        super.onResume();
        Bundle bundle = getIntent().getExtras();
        eventAdded = bundle.getBoolean("eventAdded");
        cameFromFilter = bundle.getBoolean("cameFromFilter");
        DatabaseConnector db = new DatabaseConnector(getBaseContext());
        if(cameFromFilter) {
            String typeF = findFilter.getTypeDetail();
            String dateF = findFilter.getDateDetail();
            String timeF = findFilter.getTimeDetail();
            events = db.getFilteredInfo(typeF, dateF, timeF);
        } else {
            events = db.getData();
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        gMap = googleMap;
        LatLng Ljubljana = new LatLng(46.050760, 14.502806);
        CameraPosition cameraPosition = CameraPosition.builder()
                .zoom(14).tilt(20).bearing(90).target(Ljubljana).build();
        gMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        if(eventAdded) {
            Toast.makeText(getBaseContext(), "Dodali ste dogodek!", Toast.LENGTH_LONG).show();
        }
        drawMarkers(events);
    }

    private void drawMarkers(ArrayList<String> events) {
        if(!events.isEmpty()) {
            for(String s : events) {
                String[] x = s.split("\n");
                lat = x[5];
                lng = x[6];
                naziv = x[0];
                LatLng point = new LatLng(Double.parseDouble(lat), Double.parseDouble(lng));
                gMap.addMarker(new MarkerOptions()
                        .position(point)
                        .title(naziv)
                        .snippet("Tip: " + x[1] + "\n" + "Opis: " + x[2] + "\n" + "Kdaj: " + x[3] + ", " + x[4]));

                gMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {

                    @Override
                    public View getInfoWindow(Marker arg0) {
                        return null;
                    }

                    @Override
                    public View getInfoContents(Marker marker) {

                        Context context = getBaseContext();

                        LinearLayout info = new LinearLayout(context);
                        info.setOrientation(LinearLayout.VERTICAL);

                        TextView title = new TextView(context);
                        title.setTextColor(Color.BLACK);
                        title.setGravity(Gravity.CENTER);
                        title.setTypeface(null, Typeface.BOLD);
                        title.setText(marker.getTitle());

                        TextView snippet = new TextView(context);
                        snippet.setTextColor(Color.GRAY);
                        snippet.setText(marker.getSnippet());

                        info.addView(title);
                        info.addView(snippet);

                        return info;
                    }
                });
            }
        }
    }





}
