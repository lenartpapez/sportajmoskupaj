package com.example.sebastjan.izzovime;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;



public class DatabaseConnector {
    private static String DATABASE_NAME = "newDatabase";
    private static final int DATABASE_VERSION = 1;
    private SQLiteDatabase database;                    // database object
    private DatabaseOpenHelper databaseOpenHelper;      // database helper

    // public constructor for DatabaseConnector
    public DatabaseConnector(Context context) {
        databaseOpenHelper = new DatabaseOpenHelper(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public void open() throws SQLException {
        // create or open a database for reading/writing
        database = databaseOpenHelper.getWritableDatabase();
    }

    public SQLiteDatabase read() throws  SQLException {
        database = databaseOpenHelper.getReadableDatabase();
        return database;
    }

    public void close() {
        if (database != null) database.close(); // close the database connection
    }


    public void insertData(String naziv, String tip, String opis, String datum, String cas, String lat, String lng, String zoom) {
        ContentValues newEvent = new ContentValues();
        newEvent.put("naziv", naziv);
        newEvent.put("tip", tip);
        newEvent.put("opis", opis);
        newEvent.put("datum", datum);
        newEvent.put("cas", cas);
        newEvent.put("lat", lat);
        newEvent.put("lng", lng);
        newEvent.put("zoom", zoom);
        open(); // open the database
        database.insert("events", null, newEvent);
        close(); // close the database
    }

    public void deleteData(long id) {
        open(); // open the database
        database.delete("events", "_id=" + id, null);
        close(); // close the database
    }

    public void deleteContent(){
        open();
        database.delete("events", null, null);
        close();
    }


    public ArrayList<String> getFilteredInfo(String typeF, String dateF, String timeF) {
        open();
        Cursor cursor = database.query("events", new String[]{ "_id", "naziv", "tip", "opis", "datum", "cas", "lat", "lng", "zoom"},
                "tip LIKE ? AND datum LIKE ? AND cas LIKE ?", new String[]{typeF+"%", dateF+"%", timeF+"%"}, null, null, null);
        String naziv, tip, opis, datum, cas, lat, lng;
        ArrayList<String> names = new ArrayList<String>();
        while(cursor.moveToNext()){
            naziv = cursor.getString(cursor.getColumnIndex("naziv"));
            tip = cursor.getString(cursor.getColumnIndex("tip"));
            opis = cursor.getString(cursor.getColumnIndex("opis"));
            datum = cursor.getString(cursor.getColumnIndex("datum"));
            cas = cursor.getString(cursor.getColumnIndex("cas"));
            lat = cursor.getString(cursor.getColumnIndex("lat"));
            lng = cursor.getString(cursor.getColumnIndex("lng"));
            names.add(naziv+"\n"+tip+"\n"+opis+"\n"+datum+"\n"+cas+"\n"+lat+"\n"+lng);
        }
        cursor.close();
        return names;

    }

    public ArrayList<String> getData() {
        String qry = "SELECT * FROM events";
        open();
        Cursor cursor = database.rawQuery(qry, null);
        String naziv, tip, opis, datum, cas, lat, lng;
        ArrayList<String> names = new ArrayList<String>();
        while(cursor.moveToNext()){
            naziv = cursor.getString(cursor.getColumnIndex("naziv"));
            tip = cursor.getString(cursor.getColumnIndex("tip"));
            opis = cursor.getString(cursor.getColumnIndex("opis"));
            datum = cursor.getString(cursor.getColumnIndex("datum"));
            cas = cursor.getString(cursor.getColumnIndex("cas"));
            lat = cursor.getString(cursor.getColumnIndex("lat"));
            lng = cursor.getString(cursor.getColumnIndex("lng"));
            names.add(naziv+"\n"+tip+"\n"+opis+"\n"+datum+"\n"+cas+"\n"+lat+"\n"+lng);
        }
        cursor.close();
        return names;

    }

    // get a Cursor containing all information about the contact specified by the given id
    public Cursor getOneContact(long id) {
        String iid = Long.toString(id);
        return database.query("events", null, iid, null, null, null, null);
    }

    private class DatabaseOpenHelper extends SQLiteOpenHelper {
        // public constructor
        public DatabaseOpenHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
            super(context, name, factory, version);
        }

        // creates the contacts table when the database is created
        @Override
        public void onCreate(SQLiteDatabase db) {
            String createQuery = "CREATE TABLE events"
                    + "(_id integer primary key autoincrement,"
                    + "naziv TEXT, tip TEXT, opis TEXT,"
                    + "datum TEXT, cas TEXT, lat double,"
                    + "lng double, zoom text)";

            // initializing the database
            //String insertValues = "INSERT INTO events (naziv, tip, opis, datum, cas, kraj) values ('Tek ob zici', 'Tek', 'tukej bomo tekli ob zici', '10-DEC-05', '12:00', 'Ljubljana')";
            db.execSQL(createQuery);        // execute the query
            //db.execSQL(insertValues);
        }
        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            // code for updates
        }
        @Override
        public void onDowngrade (SQLiteDatabase db, int oldVersion, int newVersion) {
            // from Android 3.0 when the database needs to be downgraded
        }
    }

}

