package com.example.sebastjan.izzovime;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import java.util.Calendar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

public class createChallenge extends AppCompatActivity implements View.OnTouchListener {
    public Button b1, b2;
    public EditText t1, t2, t3, t4, t5;
    public TextView tv;
    public int year, month, day, hour, min;
    public String[] sp4;
    public Spinner s4;
    public boolean a,b,c=false;
    public boolean locationAdded = false;
    public static boolean eventAdded = false;
    public String naziv, tip, opis, datum, cas, lat, lng, zoom;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        eventAdded = false;
        super.onCreate(savedInstanceState);
        final Context ctx = super.getBaseContext();
        setContentView(R.layout.activity_create_challenge);

        s4=(Spinner)findViewById(R.id.spinner4);
        this.sp4 = new String[]{""};

        ArrayAdapter<String> adapter4 = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, sp4);
        s4.setAdapter(adapter4);

        s4.setOnTouchListener(this);

        b1 = (Button) findViewById(R.id.create);
        b2 = (Button) findViewById(R.id.addLocation);
        t1 = (EditText) findViewById(R.id.editText);        // naziv
        t2 = (EditText) findViewById(R.id.editText2);       // opis
        t3 = (EditText) findViewById(R.id.editText3);       // datum
        t4 = (EditText) findViewById(R.id.editText4);       // ura
        tv = (TextView) findViewById(R.id.locationAdded);

        b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!t1.getText().toString().isEmpty() && !t2.getText().toString().isEmpty() && !t3.getText().toString().isEmpty()
                        && !t4.getText().toString().isEmpty()) {
                    if(locationAdded) {
                        uploadOnDB();
                        eventAdded = true;
                        Intent x = new Intent(createChallenge.this, showLocations.class);
                        x.putExtra("eventAdded", eventAdded);
                        startActivity(x);
                    }
                } else {
                    Toast.makeText(ctx, "Vsa polja so obvezna!", Toast.LENGTH_SHORT).show();
                }
            }
        });

        b2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent x = new Intent(createChallenge.this, addLocation.class);
                startActivityForResult(x, 1);
            }
        });

        t1.setOnTouchListener(this);
        t2.setOnTouchListener(this);
        t3.setOnTouchListener(this);
        t4.setOnTouchListener(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1) {
            if(resultCode == Activity.RESULT_OK){
                Toast.makeText(getBaseContext(), "Lokacija uspešno dodana.", Toast.LENGTH_SHORT).show();
                locationAdded = true;
                lat=data.getStringExtra("lat");
                lng=data.getStringExtra("lng");
                zoom=data.getStringExtra("zoom");
            }
            if (resultCode == Activity.RESULT_CANCELED) {

            }
        }
    }

    @Override
    public boolean onTouch(View v, MotionEvent event){
        if (v == t1){
            if (!a){
                t1.setText("");
                a=true;
            }
        }else if(v == t2){
            if (!b){
                t2.setText("");
                b=true;
            }
        }else if(v == t3){
            if(MotionEvent.ACTION_UP == event.getAction()){
                koledar();
                t3.setShowSoftInputOnFocus(false);              // disable tipkovnica
            }
        }else if(v == t4){
            if (MotionEvent.ACTION_UP == event.getAction()){
                ura();
                t4.setShowSoftInputOnFocus(false);              // disable tipkovnica
            }
        }else if(v == t5){
            if (MotionEvent.ACTION_UP == event.getAction()){
                if(!c){
                    t5.setText("");
                    c=true;
                }
            }
        }else if(v == s4){
            if (MotionEvent.ACTION_UP == event.getAction()) {
                this.sp4 = new String[]{"Borilne veščine", "Fitnes", "Kolesarjenje", "Košarka", "Nogomet", "Rekreacija", "Odbojka","Plavanje",
                                        "Ples", "Plezanje", "Pohodništvo", "Rokomet", "Rolanje", "Rolkanje", "Tek", "Tenis", "Drugo"};
                Spinner s4 = (Spinner) findViewById(R.id.spinner4);
                ArrayAdapter<String> adapter4 = new ArrayAdapter<String>(this, R.layout.spinner_style, R.id.weekofday, sp4);
                s4.setAdapter(adapter4);
            }
        }
        return false;
    }


    public void koledar(){
        final Calendar c = Calendar.getInstance();
        year = c.get(Calendar.YEAR);
        month = c.get(Calendar.MONTH);
        day = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog dp = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                t3.setText(dayOfMonth + "." + (month + 1) + "." + year);

            }
        }, year, month, day);
        dp.show();
    }



    public void uploadOnDB() {
        naziv = t1.getText().toString();
        tip = s4.getSelectedItem().toString();
        opis = t2.getText().toString();
        datum = t3.getText().toString();
        cas = t4.getText().toString();
        BackgroundTask backgroundTask = new BackgroundTask(this);
        backgroundTask.execute("add_info", naziv, tip, opis, datum, cas, lat, lng, zoom);
        finish();
    }

    public void ura(){
        final Calendar c = Calendar.getInstance();
        hour = c.get(Calendar.HOUR_OF_DAY);
        min = c.get(Calendar.MINUTE);
        TimePickerDialog tp = new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                String toSet = String.format("%02d:%02d", hourOfDay, minute);
                t4.setText(toSet);
            }
        }, hour, min, true);

        tp.show();
    }

}
