package com.example.sebastjan.izzovime;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Index extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_index);
        find();
        create();
    }

    public Button bt1;
    public Button bt2;
    public Button bt3;

    public void find(){
        bt1 = (Button) findViewById(R.id.findB);
        bt1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent x = new Intent(Index.this, findFilter.class);
                startActivity(x);
            }
        });
    }

    public void create(){
        bt2 = (Button) findViewById(R.id.createB);
        bt2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent x = new Intent(Index.this, createChallenge.class);
                startActivity(x);
            }
        });

    }


}
